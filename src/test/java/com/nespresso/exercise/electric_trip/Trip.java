/*
 * *************************************************************************
 * Copyright (C) Wolters Kluwer Financial Services. All rights reserved.
 *
 * This computer program is protected by copyright law and international 
 * treaties. Unauthorized reproduction or distribution of this program, 
 * or any portion of it, may result in severe civil and criminal penalties, 
 * and will be prosecuted to the maximum extent possible under the law.
 * *************************************************************************
 */
package com.nespresso.exercise.electric_trip;

public class Trip {

    private String from;
    private float distance;
    private String to;
    private int chargeRate;

    public String getFrom() {
        return from;
    }

    public float getDistance() {
        return distance;
    }

    public String getTo() {
        return to;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getChargeRate() {
        return chargeRate;
    }

    public void setChargeRate(int chargeRate) {
        this.chargeRate = chargeRate;
    }
}