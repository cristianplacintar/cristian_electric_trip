/*
 * *************************************************************************
 * Copyright (C) Wolters Kluwer Financial Services. All rights reserved.
 *
 * This computer program is protected by copyright law and international 
 * treaties. Unauthorized reproduction or distribution of this program, 
 * or any portion of it, may result in severe civil and criminal penalties, 
 * and will be prosecuted to the maximum extent possible under the law.
 * *************************************************************************
 */
package com.nespresso.exercise.electric_trip;

public class Participant {

    private String location;
    private final int batterySize;
    private final int lowSpeedPerformance;
    private final int highSpeedPerformance;

    private float batteryLeft;

    private int currentTripIndex;

    public Participant(String location, int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
        this.location = location;
        this.batterySize = batterySize;
        this.batteryLeft = batterySize;
        this.lowSpeedPerformance = lowSpeedPerformance;
        this.highSpeedPerformance = highSpeedPerformance;
        currentTripIndex = 0;
    }

    public String getLocation() {
        return location;
    }

    public String charge() {
        final int chargeValue = Math.round(batteryLeft / batterySize * 100);
        return chargeValue + "%";
    }

    public int getCurrentTripIndex() {
        return currentTripIndex;
    }

    public void setCurrentTripIndex(int currentTripIndex) {
        this.currentTripIndex = currentTripIndex;
    }

    public int getBatterySize() {
        return batterySize;
    }

    public int getLowSpeedPerformance() {
        return lowSpeedPerformance;
    }

    public int getHighSpeedPerformance() {
        return highSpeedPerformance;
    }

    public float getBatteryLeft() {
        return batteryLeft;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setBatteryLeft(float batteryLeft) {
        this.batteryLeft = batteryLeft;
    }
}