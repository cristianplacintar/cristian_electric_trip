/*
 * *************************************************************************
 * Copyright (C) Wolters Kluwer Financial Services. All rights reserved.
 *
 * This computer program is protected by copyright law and international 
 * treaties. Unauthorized reproduction or distribution of this program, 
 * or any portion of it, may result in severe civil and criminal penalties, 
 * and will be prosecuted to the maximum extent possible under the law.
 * *************************************************************************
 */
package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.List;

public class ElectricTrip {
    private final List<Trip> trips = new ArrayList<>();

    private final List<Participant> participants = new ArrayList<>();

    public ElectricTrip(String tripInfo) {
        String[] tokens = tripInfo.split("-");
        Trip trip = new Trip();
        boolean isLocation = true;
        boolean nextOne = false;
        for (int i = 0; i < tokens.length; i++) {
            if (isLocation) {
                if (nextOne) {
                    String[] splitOnCharge = tokens[i].split("\\:");
                    trip.setTo(splitOnCharge[0]);
                    boolean chargeableLocation = splitOnCharge.length > 1;
                    if (chargeableLocation) {
                        trip.setChargeRate(Integer.valueOf(splitOnCharge[1]));
                    }
                    trips.add(trip);

                    trip = new Trip();
                    nextOne = false;
                }
                trip.setFrom(tokens[i]);
                isLocation = false;
            } else {
                trip.setDistance(Integer.valueOf(tokens[i]));
                isLocation = true;
                nextOne = true;
            }
        }
    }

    public int startTripIn(String location, int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
        final Participant participant = new Participant(location, batterySize, lowSpeedPerformance, highSpeedPerformance);
        for (int i = 0; i < trips.size(); i++) {
            if (trips.get(i).getFrom().equals(location)) {
                participant.setCurrentTripIndex(i);
            }
        }
        participants.add(participant);

        return participants.size() - 1;
    }

    public void go(int participantId) {
        final Participant participant = participants.get(participantId);
        goWithSpeed(participant, participant.getLowSpeedPerformance());
    }

    private void goWithSpeed(Participant participant, int speedPerformance) {
        for (int i = participant.getCurrentTripIndex(); i < trips.size() ; i++) {
            final Trip trip = trips.get(i);

            float distance = trip.getDistance();
            float batteryLeft = participant.getBatteryLeft();

            final float batteryConsumed = distance / speedPerformance;
            if (batteryConsumed <= batteryLeft) {
                batteryLeft -= batteryConsumed;
                distance -= batteryConsumed * speedPerformance;

                if (distance <= 0) {
                    participant.setLocation(trip.getTo());
                    participant.setBatteryLeft(batteryLeft);
                }
            }
        }
    }

    public String locationOf(int participantId) {
        return participants.get(participantId).getLocation();
    }

    public String chargeOf(int participantId) {
        return participants.get(participantId).charge();
    }

    public void sprint(int participantId) {
        final Participant participant = participants.get(participantId);
        goWithSpeed(participant, participant.getHighSpeedPerformance());
    }

    public void charge(int id, int hoursOfCharge) {

    }
}